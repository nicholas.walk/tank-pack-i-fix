# [[Simfphys Armed] Tank Pack I [Update]](https://steamcommunity.com/sharedfiles/filedetails/?id=1586745401) (FIX)
This fixes the non working turrets on the following vehicles:
 - sim_fphys_tank_jumbo
 - sim_fphys_tank_jumbo_rp
 - sim_fphys_tank_panzeriv
 - sim_fphys_tank_panzeriv_rp

To apply the fix, simply place the folder inside of your server or clients addons directory.
 - garrysmod
    - addons
        - tank-pack-i-fix

**[NOTE]:** This is not a replacement for the [workshop addon](https://steamcommunity.com/sharedfiles/filedetails/?id=1586745401) and requires it to be installed alongside this fix.